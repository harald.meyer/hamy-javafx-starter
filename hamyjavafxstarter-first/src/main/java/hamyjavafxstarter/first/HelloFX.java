package hamyjavafxstarter.first;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.util.Date;

public class HelloFX extends Application {

    @Override
    public void start(Stage stage) {
        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        String cn = getClass().getName();
        Label l = new Label(
                "Main class: " + cn + "\n\nHello, JavaFX " + javafxVersion + ", running on Java " + javaVersion +
                        ".\n\nScreenshot taken at " + new Date());
        Scene scene = new Scene(new StackPane(l), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}